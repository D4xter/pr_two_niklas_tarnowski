namespace PRTWO
{
    public interface IDamagable
    {
        /// <summary>
        /// Method for damage calculation, has an int parameter
        /// </summary>
        /// <param name="damageAmount"></param>
        void Damage(int Amount);
    }
}